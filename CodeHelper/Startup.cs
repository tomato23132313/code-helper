using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using RazorEngine;
using RazorEngine.Templating; // For extension methods.

namespace CodeHelper
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //services.AddControllersWithViews();
            services.AddMvc().AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            //注册模板中间件
            app.Use(async (context, next) =>
            {
                var templateName = Configuration.GetSection("DbConfigInfo")["TemplateName"];
                var filePath = Path.Combine(env.WebRootPath, "templates", templateName);

                if (!File.Exists(filePath))
                {
                    await context.Response.WriteAsync("模板文件不存在");
                }
                else {

                    //打开并且读取模板
                    string template = File.ReadAllText(filePath);
                    //添加模板
                    Engine.Razor.AddTemplate("templateKey", template);
                    //编译模板
                    Engine.Razor.Compile("templateKey", null);

                    await next.Invoke();
                }
               
            });

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
