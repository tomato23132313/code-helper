﻿using CodeHelper.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeHelper.Manager
{
    public class ConfigManager
    {
        public static DbConfigInfo GetDbConnList(IConfiguration configuration)
        {
            DbConfigInfo dbConfigInfo = new DbConfigInfo();
            configuration.GetSection("DbConfigInfo").Bind(dbConfigInfo);

            return dbConfigInfo;
        }
    }
}
