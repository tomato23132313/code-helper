﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace CodeHelper.Common
{
    public class DbHelper
    {
        public static string ConnInUse;

        public static void TestConn(string connStr)
        {
            try
            {
                ConnInUse = connStr;
                var conn = new MySqlConnection(connStr);
                conn.Open();
                conn.Close();
            }
            catch (Exception)
            {
                throw new Exception("数据库链接失败！");
            }
        }

        public static IDbConnection GetConn()
        {

            try
            {
                return new MySqlConnection(ConnInUse);
            }
            catch (Exception)
            {
                throw new Exception("数据库链接失败！");
            }
        }
    }
}
