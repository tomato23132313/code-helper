﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using CodeHelper.Models;
using Microsoft.Extensions.Configuration;
using CodeHelper.Common;
using CodeHelper.Manager;
using Microsoft.AspNetCore.Hosting;

namespace CodeHelper.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IConfiguration _configuration;

        public HomeController(ILogger<HomeController> logger, IConfiguration configuration)
        {
            _logger = logger;
            _configuration = configuration;
        }

        public IActionResult Index()
        {
            //验证数据库链接
            DbConfigInfo dbConfigInfo = ConfigManager.GetDbConnList(_configuration);
            string connInUse = dbConfigInfo.ConnList.Find(x => x.InUse == true)?.ConnStr;
            DbHelper.TestConn(connInUse);

            //初始化传参
            List<string> dbs = ModelManager.GetDbs();

            ViewBag.Dbs = dbs;

            //默认显示第一个数据库的所有表
            ViewBag.Tables = ModelManager.GetTables(dbs[0]);

            return View();
        }
    }
}
