﻿using CodeHelper.Manager;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.IO;

namespace CodeHelper.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class ModelController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IWebHostEnvironment _webHostEnvironment;
        public ModelController(IConfiguration configuration, IWebHostEnvironment webHostEnvironment)
        {
            _configuration = configuration;
            _webHostEnvironment = webHostEnvironment;
        }

        /// <summary>
        /// 根据数据库名获取其下所有表名
        /// </summary>
        /// <param name="dbName">数据库名</param>
        /// <returns></returns>
        [HttpGet]
        public List<string> GetTableList(string dbName)
        {
            return ModelManager.GetTables(dbName);
        }

        [HttpGet]
        public string GenerateCode(string dbName, string tableName)
        {
            return ModelManager.GenerateModel(dbName,tableName);
        }
    }
}
