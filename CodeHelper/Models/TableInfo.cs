﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeHelper.Models
{
    public class TableInfo
    {
        public TableInfo(string tableName)
        {
            TableName = tableName;
        }

        /// <summary>
        /// 表名
        /// </summary>
        public string TableName { get; set; }

        /// <summary>
        /// 参数字段
        /// </summary>
        public List<DbParamInfo> Parameters { get; set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Desc { get; set; }
    }
}
