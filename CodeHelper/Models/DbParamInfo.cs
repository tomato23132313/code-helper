﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeHelper.Models
{
    public class DbParamInfo
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        private string paramName;

        /// <summary>
        /// 字段类型
        /// </summary>
        private string paramType;

        /// <summary>
        /// 字段描述
        /// </summary>
        private string desc;


        public string ParamName
        {
            get { return paramName; }
            set { paramName = value; }
        }

        public string ParamType
        {
            get { return paramType; }
            set { paramType = value; }
        }

        public string Desc
        {
            get { return desc; }
            set { desc = value; }
        }
    }
}
