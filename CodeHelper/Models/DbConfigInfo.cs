﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CodeHelper.Models
{
    public class DbConfigInfo
    {
        public List<ConnInfo> ConnList { get; set; }
    }

    /// <summary>
    /// 链接字符串信息
    /// </summary>
    public class ConnInfo
    {
        public string ConnStr { get; set; }
        public bool InUse { get; set; }
    }
}
