# CodeHelper

#### 介绍
.Net Core3.1 使用 RazorEngine.NetCore 制作实体生成器 （MVC网页版）

#### 使用说明

1.模板文件存放路径 /wwwroot/templates

2.修改配置文件 appsettings.json

详细介绍：https://blog.csdn.net/Tomato2313/article/details/109844812

```
{
  "Logging": {
    "LogLevel": {
      "Default": "Information",
      "Microsoft": "Warning",
      "Microsoft.Hosting.Lifetime": "Information"
    }
  },
  "AllowedHosts": "*",
  "DbConfigInfo": {
    "TemplateName": "modelTest.cshtml",
    "ConnList": [
      {
        "ConnStr": "你的数据库链接字符串1",
        "InUse": true
      },
      {
        "ConnStr": "你的数据库链接字符串2",
        "InUse": false
      }
    ]
  }
}

```

InUse 表示正在使用的配置

配置完成后，直接启动项目，并访问相应地址
效果：


![输入图片说明](https://images.gitee.com/uploads/images/2020/1120/155027_426787b7_7401018.png "屏幕截图.png")

